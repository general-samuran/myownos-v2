# Objetivo: Programar GDT

En este apartado tenemos que hacer mención a la segmentación de la lección 6, que en el modo de 32 bits funciona de forma diferente. Ahora, el registrador del segmento se convierte en un indice de un descriptor de segmento (SD, de las siglas de `segment descriptor` en inglés). Este descriptor define la dirección base (32 bits), el tamaño (20 bits) y otros indicadores (flags) que indican los privilegios del código o si es de sólo lectura o escritura.

En el siguiente [enlace](https://en.wikipedia.org/wiki/File:SegmentDescriptor.svg) se puede ver la estructura de un descriptor de segmento.

La forma más sencilla de programar la GDT es definir 2 segmentos, una para el código y otro para los datos. Estos se pueden superponer, lo que quitaría la protección de memoria, pero que nos valdría para arrancar. Más tarde lo arreglaremos usando otro lenguaje.

La primera entrada de la GDT debe ser `0x0`, un descriptor nulo (una estructura de 8 cero bytes). Este es un mecanismo para evitar que el desarrollador cometa ningún error al gestionar las direcciones después de cambiar al modo protegido.

Además, la CPU no puede cargar directamente la dirección GDT, por lo que necesita una metaestructura de 6 bytes llamada "descriptor GDT" con el tamaño (16 bits) y la dirección (32 bits) de nuestra GDT real. Se carga con `lgdt`.

El segmento de código tendrá la siguiente configuración:

- Base: 0x0
- Limit: 0xfffff
- Present: 1
- Privilige: 0, 0 para privilegios elevados
- Descriptor type: 1 para segmento de código o datos, 0 para traps(?)
- Type:
  - Code: 1 para código, desde aquí es un segmento de código.
  - Conforming: 0
  - Readable: 1, 1 para que se pueda leer, 0 si es sólo ejecución.
  - Accessed: 0, usado para hacer debug.
- Otras flags:
  - Granularity: 1, si está configurado esto multiplica su límite por 4 K (es decir, 16*16*16), por lo que nuestro 0xffff se convertiría en 0xfffff000 (es decir, desplaza 3 dígitos hexadecimales a la izquierda), lo que permite que nuestro segmento abarque 4 Gb de memoria.
  - 32-bit default: 1
  - 64-bit default: 0, no se usa en el modo 32-bit
  - AVL: 0, podemos usarlo para hacer debug, por ejemplo, pero nosotros no lo usaremos.

El segmento de datos es identico, cambiando estos type flags:

- Code: 0, para datos
- Expand down: 0, usado para permitir que el segmento se expanda para abajo.
- Writable: 1, permite escribir en el segmento de datos, de lo contrario sería sólo lectura.
- Accessed: 0, se utiliza para hacer debugging y técnicas de memoria virtual.

En el fichero [32bit-gdt.asm](./32bit-gdt.asm) veremos todo esto que hemos comentado.

En la siguiente lección, cambiaremos al modo protegido de 32 bits y utilizaremos el código de estas lecciones.
