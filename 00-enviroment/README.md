# Objetivo: Instalar el software necesario

Tenemos que instalar los siguientes paquetes `sudo apt install qemu nasm`

Dentro de qemu hay varios ejecutables, nosotros usaremos `qemu-system-x86_64 fichero`

Para no tener que escribir el comando entero, en mi caso he creado un alias para que al ejecutar `qemu` haga la misma función. A continuación se muestra cómo hacerlo para el usuario con el que trabajamos.

```bash
# Si tenemos zsh, el fichero modificar sería ~/.zshrc. En este caso sería para bash
vi ~/.bashrc 

# Dentro del fichero añadimos lo siguente, al final del mismo para llevar un orden.
alias qemu='qemu-system-x86_64'

# Guardamos y cerramos el fichero. Activamos el alias con el comando source. Lo comentado anteriormente, si tenemos zsh, lo ejecutamos con~/.zshrc 
source ~/.bashrc
```
