# Objetivo: Preparar nuestro entorno de desarrollo para construir nuestro kernel

**Nota: Puede que falte por apuntar alguna dependencia por haber estado haciendo pruebas**
**Nota 2: Hay que ejecutar los make como root o con sudo, ya que si no dará errores de permisos**
Vamos a necesitar `binutils` y `gcc`, que ya vienen instalados en Ubuntu, pero necesitamos configurarlo de cierta manera, así que vamos a seguir estos pasos para poder hacerlo.

Configuramos las rutas que vamos a utilizar, hemos utilizado `/usr/local/i386elfgcc` para evitar colisiones con el compilador y binutils que ya vienen instalados.

```bash
export TARGET=i386-elf
export PREFIX="/usr/local/i386elfgcc"
export PATH="$PREFIX/bin:$PATH"
```

Recomiendo poner el segundo y el tercer export en el fichero `~/.bashrc` o `~/.zshrc` para que se carguen automaticamente.

## Instalar binutils

Voy a ponerlo para que sea copiar las líneas y lo haga todo automatico:

```bash
sudo apt install texinfo -y
mkdir /tmp/src
cd /tmp/src
curl -O http://ftp.gnu.org/gnu/binutils/binutils-2.40.tar.gz # Revisad antes la última versión disponible
tar xf binutils-2.40.tar.gz
mkdir binutils-build
cd binutils-build
../binutils-2.40/configure --target=$TARGET --enable-interwork --enable-multilib --disable-nls --disable-werror --prefix=$PREFIX 2>&1 | tee configure.log
sudo make all install 2>&1 | tee make.log
```

## Instalar gcc

Para este vete a por unas pipas, que tarda lo suyo.

```bash
cd /tmp/src
curl -O https://ftp.gnu.org/gnu/gcc/gcc-13.1.0/gcc-13.1.0.tar.gz
tar xf gcc-13.1.0.tar.gz
cd gcc-13.1.0
./contrib/download_prerequisites # Esto es para que descargue ciertas librerias que necesita sin que tengamos que hacerlo nosotros con apt install
cd ..
mkdir gcc-build
cd gcc-build
../gcc-13.1.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --disable-libssp --enable-languages=c --without-headers
sudo make all-gcc
sudo make all-target-libgcc
sudo make install-gcc
sudo make install-target-libgcc
```
