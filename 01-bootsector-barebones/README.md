# Objetivo: Crear un fichero que la BIOS interprete como un disco booteable

## Teoria

Cuando el ordenador arranca, la BIOS no sabe cómo cargar el sistema operativo, así que delega esa tarea al sector de arranque. Por lo tanto, este sector debe colocarse en una ubicación estándar conocida.

Para asegurarse que el disco sea booteable, la BIOS comprueba que los bytes 511 y 512 del sector de arranque sean bytes `0xAA55`.

Este es el sector de arranque más simple que existe:

```text
e9 fd ff 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
[ 29 más líneas con 16 cero bytes en cada una ]
00 00 00 00 00 00 00 00 00 00 00 00 00 00 55 aa
```

Son todos ceros, terminado con el valor de 16 bits `0xAA55`. Los primeros 3 bytes son un salto infinito.

## El sector de arranque más simple

Tenemos dos opciones, escribir los 512 bytes anteriores a mano o escribir este código en ensamblador:

```asm
; Loop infinito (e9 fd ff)
loop:
    jmp loop

; Rellenamos con 510 ceros menos el tamaño del codigo anterior
times 510-($-$$) db 0

;Numero mágico, con "dw" declarmos 2 bytes
dw 0xaa55
```

Para compilar: `nasm -f bin boot_sect_simple.asm -o boot_sect_simple.bin`

Para ejecutar: `qemu boot_sect_simple.bin`

Nos aparecerá una pantalla que pone "Booting from Hard Disk..." y ya. Eso es todo por el momento, el programa hace un bucle infinito que hace que aparezca ese mensaje todo el rato.
