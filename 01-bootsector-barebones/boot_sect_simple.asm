; Loop infinito (e9 fd ff)
loop:
    jmp loop

; Rellenamos con 510 ceros menos el tamaño del codigo anterior
times 510-($-$$) db 0

;Numero mágico, con "dw" declarmos 2 bytes
dw 0xaa55