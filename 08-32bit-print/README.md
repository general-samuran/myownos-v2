# Objetivo: Imprimir por pantalla usando el modo protegido 32-bit

Con el modo 32 bits nos permite usar registros de 32 bits, además de direccionamientos de memoria, protección de memoria, memoria virtual y más, pero a cambio de perder las interrupciones de la BIOS y necesitamos utilizar GDT (de las siglas `Global Descriptor Table`), que hablaremos más adelante.

Las principales diferencias son:

- Los registradores tienen como prefijo una `e`, por ejemplo `mov ebx, 0x274fe8fe`
- Se agregan dos registradores más, `fs` y `gs` (se comentó en ocasiones anteriores).
- Podemos utilizar hasta 4GB de memoria.
- La CPU se vuelve más sofistica, a cambio de volverse más complicada, en cuanto a segmentación de memoria.

En esta lección vamos a escribir un código que imprima texto por pantalla, sin usar las interrupciones de la BIOS, manipulando directamente el VGA, cuya dirección de memoria empieza en `0xb8000` y tiene un modo texto que nos va a ser útil para evitar manipular píxeles.

La formula para acceder a un caracter especifico en una cuadricula de 80x25 es:

`0xb8000 + 2 * (row * 80 + col)`

Cada caracter usa 2 bytes de memoria, 1 byte para el ASCII y 1 byte para los atributos del mismo, como el color.

En el fichero [32bit-print.asm](./32bit-print.asm) podremos ver un ejemplo en el que siempre se imprimirá el texto en la parte superior de la pantalla.

De momento no podremos ejecutar este código ya que todavía no sabemos cómo escribir la GDT e ingresar en el modo protegido.
