mov ah, 0x0e ; modo tty
mov al, 'H'
int 0x10
mov al, 'e'
int 0x10
mov al, 'l'
int 0x10
int 0x10 ; 'l' sigue todavía en al
mov al, 'o'
int 0x10

jmp $; Creamos el bucle infinito

; Rellenamos con 510 ceros menos el tamaño del codigo anterior y ponemos el número mágico
times 510-($-$$) db 0
dw 0xaa55