# Objetivo: Mostrar texto en nuestro sector de arranque

En este apartado vamos a hacer que se muestre la palabra 'Hello' en el sector de arranque.

Para ello vamos a escribir caracter por caracter en el registro `al` (la parte baja de `ax`), los bytes `0x0e` en `ah` (la parte alta de `ax`) y mandaremos una interrupción `0x10`, que es una [interrupción que controla los servicios de pantalla/video del PC](https://es.wikipedia.org/wiki/Int_10h).

`0x0e` en `ah` le indica a la interrupción de video (`0x10`) que queremos mostrar caracteres que hemos guardado en `al`, también llamado 'modo TTY'.

Para este caso sólo vamos a declarar el modo TTY una vez, aunque en un caso real no podemos asegurar que el contenido de `ah` sea constante. Podría ser que otro proceso se ejecutase en la CPU, no se limpie correctamente y deje basura en `ah`.

Para este ejemplo no es necesario preocuparse, ya que sólo nosotros usamos la CPU.

El código resultante es el que se encuentra en el fichero [boot_sect_hello.asm](./boot_sect_hello.asm)

Generamos el binario y lo ejecutamos.

```bash
nasm -f bin boot_sect_hello.asm -o boot_sect_hello.bin
qemu boot_sect_hello.bin
```

Podremos ver el contenido del binario generado con `xxd fichero.bin`

## Resumen

- Escribimos los caracteres en `al`, la parte baja de `ax`.
- Los bytes `0x0e` en `ah` (la parte alta de `ax`) sirven para entrar en el modo TTY o modo consola, mostramos caracteres.
- Los bytes `0x10` es una interrupción general que gestiona video.
- Más información sobre los [tipos de 10h](https://es.wikipedia.org/wiki/Int_10h)
