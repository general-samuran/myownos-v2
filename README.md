# myownos-v2

Esto es un fork de <https://github.com/cfenollosa/os-tutorial>, en su día hice un proyecto parecido, pero quiero darle un lavado de cara y dejarlo mejor documentado.

Links de interés que he visto

- Este [PDF](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf) que explica bastante al respecto

- La wiki de [OSDev.org](https://wiki.osdev.org/Main_Page)

- Concepto básicos del [lenguaje ensamblador](https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_lenguaje_ensamblador/Primeros_conceptos)

- Para convertir ASCII en hexadecimal y al revés. [Link](https://www.rapidtables.com/convert/number/ascii-to-hex.html)

Otros links que recomienda el autor

- <https://littleosbook.github.io/>

- <https://web.archive.org/web/20160412174753/http://www.jamesmolloy.co.uk/tutorial_html/index.html>
