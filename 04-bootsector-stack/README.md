# Objetivo: Aprender cómo funciona la pila o stack

La pila, o stack a partir de ahora, es una solución para el siguiente problema: la CPU tiene un nº limitado de registros para almacenar de forma temporal las variables de nuestro código, pero a veces es necesario más almacenamiento temporal del esperado. Se podría utilizar la memoria principal, pero especificar direcciones de memorias especificas para la lectura y escritura es algo engorroso, ya que no nos importa dónde se van a almacenar esos datos, sólo que podamos recuperarlos de forma sencilla.
También es útil para pasar argumentos para realizar llamadas a funciones.

La CPU tiene 2 instrucciones, `push` y `pop`, que nos permiten almacenar y recuperar, respectivamente, un valor de la parte superior del stack, sin tener que preocuparnos de dónde están.

Una cosa a tener en cuenta es que no se pueden almacenar bytes individuales, si estamos en modo 16 bits, sólo funcionará en los limites de 16 bits.

Para esto existen 2 registros especiales, `bp` y `sp`, que manejan la parte inferior y superior del stack, respectivamente. Como el stack va creciendo según vamos añadiendo datos, tenemos que configurarla lejos de regiones importantes de la memoria (como la BIOS o nuestro código) para que no haya peligro de sobreescribir el stack si este crece demasiado.
Una cosa a tener en cuenta, es que el stack crece hacia abajo desde el puntero base, por lo que los valores se almacenan debajo de la dirección de `bp`, mientras que `sp` se va reduciendo.

Quizás todo esto sea algo lioso, en el código [boot_sect_stack.asm](boot_sect_stack.asm) se encuentran varios ejemplos y comentarios que ayudarán a entender esto mucho mejor.

Si vemos el contenido del binario, podremos observar cómo las letras están en el orden que le hemos puesto, primero la 'A', luego la 'B' y por último la 'C', pero a la hora de ejecutarlo, nos lo mostrará en el sentido contrario por lo comentado de que el stack crece desde abajo.
