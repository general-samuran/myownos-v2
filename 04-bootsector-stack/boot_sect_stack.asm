mov ah, 0x0e ; modo tty

mov bp, 0x8000 ; Ponemos la parte baja (bp) del stack en un sector alejado de 0x7c00 para no sobreescribirlo
mov sp, bp ; Como el stack está vacío hacemos que la parte superior (sp) apunte a la parte baja (bp)

push 'A' ; Almacenamos los datos en la parte superior del stack
push 'B'
push 'C'

; Vamos a recuperar los caracteres con `pop`
; Sólo podemos mostrar 16 bits, así que pasamos a bx para manipular bl (8 bits)

pop bx
mov al, bl
int 0x10 ; Mostrará C

pop bx
mov al, bl
int 0x10 ; Mostrará B

mov al, [0x7ffe] ; Para demostrar que el stack crece desde abajo de bp, 
                 ; buscamos el caracter en 0x8000 - 0x2 (16 bites)

int 0x10 ; Mostrará A

jmp $ ; Bucle infinito

; Relleno de ceros y número mágico
times 510-($-$$) db 0
dw 0xaa55
