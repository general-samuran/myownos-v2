#include "../drivers/screen.h"

void main(){
    clear_screen();
    kprint_at("X", 1, 6);
    kprint_at("Esto es un texto con multiples lineas", 75, 10);
    kprint_at("Esto es una linea\nbreak", 0, 20);
    kprint("Esto es una linea\nbreak");
    kprint_at("¿Qué pasa si nos salimos del espacio proporcionado?", 45, 24);
}