[bits 32] ; Usando el modo protegido

; definimos las constantes
VIDEO_MEMORY equ 0xb8000
WHITE_ON_BLACK equ 0x0f ; el color de cada byte

print_string_pm:
    pusha
    mov edx, VIDEO_MEMORY

print_string_pm_loop:
    mov al, [ebx] ; [ebx] es la dirección de nuestro caracter
    mov ah, WHITE_ON_BLACK

    cmp al, 0 ; comprueba si es el final del string
    je print_string_pm_done

    mov [edx], ax ; almacena el caracter y el atributo en la memoria del video
    add ebx, 1 ; siguiente caracter
    add edx, 2 ; siguiente posicion en la memoria de video

    jmp print_string_pm_loop

print_string_pm_done:
    popa
    ret
