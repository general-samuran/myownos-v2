print:
    pusha

; Esto es como el siguiente while:
; while (string[i] !=0) {print string[i]; i++}

; La comparación la realiza con el final del string (null byte), que es 0

start:
    mov al, [bx] ; bx es la dirección base para el string
    cmp al, 0 ; Comprueba si el valor que obtiene es igual a 0 y si es así
    je done   ; va a la función que deja los valores originales y devuelve todos los datos recogidos

    ; Esta parte es la que nos permite mostrar caracteres, como hemos hecho en ocasiones anteriores
    mov ah, 0x0e
    int 0x10

    ; Incrementamos el puntero y vamos al siguiente loop si es necesario
    add bx, 1
    jmp start

done:
    popa
    ret

; Para el salto de línea
print_nl:
    pusha
    mov ah, 0x0e
    mov al, 0x0a ; caracter de nueva linea
    int 0x10
    mov al, 0x0d ; caracter de retorno de carro
    int 0x10

    popa
    ret

