# Objetivo: Aprender cómo funciona la memoria con segmentación en el modo real de 16 bits

Esta lección es para repasar conceptos que dimos en la lección 3.

La segmentación lo hicimos con `[org]`. La segmentación se utiliza para dividir la memoria del sistema en distintos grupos independientes, llamados segmentos (valgame la redundancia). Cada segmento se utiliza para contener un tipo específico de datos.

Esto se hace mediante registradores especiales: `cs`, `ds`, `ss` y `es`, código, datos, stack y extra, respectivamente (el último es para lo que defina el usuario). Existen otros 2, `fs` y `gs` que se definen como uso general.

Una cosa importante a tener en cuenta es que si asignamos un valor para un registro de segmento determinado, como `ds`, ese valor se usará para todas las demás operaciones de acceso a memoria de nuestro código. Esto es, que cargar o almacenar datos se verán afectados por ese registro de segmento.

Para calcular la dirección real, no sólo concatenamos las direcciones del segmento y el desplazamiento, sino que se produce un solapamiento entre ellos: `segment << 4 + address`. Por ejemplo, si `ds` es `0x4d`, entonces `[0x20]` se refiere a `0x4d0 + 0x20 = 0x4f0`.  
Esto es debido a que se desplaza a la izquierda 4 bits, añadiendo un '0' al final del valor que se encuentra en el registro.

Otros datos a tener en cuenta sobre la segmentación es que tiene un máximo de memoria de 1MB y no tiene protección de memoria, donde arquitectura más modernas como la x86 sí cuenta.

En el ejemplo [boot_sect_segmentation.asm](boot_sect_segmentation.asm) se podrá ver mejor cómo funciona lo comentado hasta ahora.
