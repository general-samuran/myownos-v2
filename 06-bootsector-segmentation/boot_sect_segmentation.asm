mov ah, 0x0e ; modo tty

mov al, "1"
int 0x10
mov al, [the_secret]
int 0x10 ; al igual que el ejemplo 2 de la lección 3, esto no funcionará

mov al, "2"
int 0x10
mov bx, 0x7c0 ; el segmento hará el desplazamiento automaticamente por nosotros
mov ds, bx ; no podemos setear ds directamente, así que configuramos bx y copie su contenido en ds
mov al, [the_secret]
int 0x10

mov al, "3"
int 0x10
mov al, [es:the_secret] ; decimos a la CPU que utilice el segmento es,
int 0x10                ; esto tampoco funcionará, ya que ese registro tiene como valor 0x000

mov al, "4"
int 0x10
mov bx, 0x7c0
mov es, bx
mov al, [es:the_secret]
int 0x10

jmp $ ; Bucle infinito

the_secret:
    db "X"

; Relleno de ceros y número mágico
times 510-($-$$) db 0
dw 0xaa55