[org 0x7c00]
    mov bp, 0x9000 ; Configuramos el stack
    mov sp, bp

    mov bx, MSG_REAL_MODE
    call print  ; Mostramos esto después del mensaje de la BIOS

    call switch_to_pm
    jmp $

%include "../05-bootsector-functions-strings/boot_sect_print.asm"
%include "../09-32bit-gdt/32bit-gdt.asm"
%include "../08-32bit-print/32bit-print.asm"
%include "32bit-switch.asm"

[bits 32]
BEGIN_PM:
    mov ebx, MSG_PROT_MODE
    call print_string_pm ; Usamos el metodo para pintar caracteres de 32 bits, que lo mostrará
                         ; en la parte superior izquierda
    jmp $

MSG_REAL_MODE db "Iniciado modo real 16-bit", 0
MSG_PROT_MODE db "Cargado modo protegido 32-bit", 0

; Bootloader y número mágico
times 510-($-$$) db 0
dw 0xaa55
