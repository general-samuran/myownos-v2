# Objetivo: Entrar en el modo protegido de 32-bit y probar el código de lecciones anteriores

Para entrar en el modo 32-bit tenemos que:

1. Deshabilitar las interrupciones
2. Cargar nuestro GDT con `lgdt`
3. Establecer un bit en el control de registro de la CPU `cr0`
4. Limpiar el pipeline de la CPU (se utiliza para permitir las ejecuciones en paralelo) y saltar a un segmento diferente, lejos de donde trabajaremos.
5. Actualizar todos los registros del segmento.
6. Actualizar el stack.
7. Llamar al código de 32 bits.

_Cómo hacer el paso 3._

```asm
mov eax, cr0
or eax, 0x1
mov cr0, eax
```

Todo esto estará en el código [32bit-switch.asm](./32bit-switch.asm).

Después de ingresar al modo 32-bit, llamaremos a `BEGIN_PM`, el cual es el punto de entrada para nuestro código. El fichero es [32bit-main.asm](./32bit-main.asm), que si lo compilamos y ejecutamos, veremos 2 mensajes en pantalla.

La siguiente lección será escribir un kernel simple.
