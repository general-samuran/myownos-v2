[org 0x7c00] ; Nos posicionamos en el sector de arranque

; La rutina principal se asegura que los parametros están preparados y entonces llama a las funciones

mov bx, HELLO
call print

call print_nl

mov bx, GOODBYE
call print

call print_nl

mov dx, 0x12fe ; guardamos el valor para imprimir en dx
call print_hex

jmp $

; Incluimos los dos ficheros con las funciones
%include "boot_sect_print.asm"
%include "boot_sect_print_hex.asm"

; los datos
HELLO:
    db 'Hola mundo', 0

GOODBYE:
    db 'Adios', 0

; Relleno de ceros y número mágico
times 510-($-$$) db 0
dw 0xaa55