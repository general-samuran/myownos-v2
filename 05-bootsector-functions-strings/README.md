# Objetivo: Aprender a codificar cosas básicas (strings, funciones, bucles) en ensamblador

En las siguientes lecciones ya programaremos nuestro sector de arranque.

En esta lección aprenderemos a escribir código con estructuras de control (aka bucles/condicionales), llamadas a funciones y uso de strings.

## Strings

Para definir un string, o cadena de texto, tenemos que hacerlo de la siguiente forma:

```asm
my_string:
    db 'Hola mundo', 0
```

`db` vendría a ser "declarar byte(s) de datos", donde le decimos que escriba directamente la secuencia de bytes en el binario. Lo que pongamos entre comillas hará que el ensamblador interprete cada caracter en código byte ASCII. Con la etiqueta `my_string` marcamos el comienzo de los datos, para poder hacer referencia a ello en otras partes del código de forma más cómoda.

El 0 del final se interpreta como el byte `0x00` (byte nulo). De esta forma declaramos un _null-terminating_

## Estructuras de control (bucles/condicionales)

Esto vendría a ser `if..then..elseif..else`, `for` y `while`. Los condicionales y bucles que usamos en programacion.

En los anteriores códigos hemos venido usando `jmp $` para hacer el bucle infinito, porque estamos haciendo que vaya siempre a la instrucción actual una y otra vez.

Estos saltos vienen definidos por el resultado de la instrucción anterior, si en la ejecución anterior ha devuelto un 4, por ejemplo, en el siguiente paso haz un paso, pero si ha devuelto otro valor, haz otro paso y así. Un ejemplo:

```asm
cmp ax, 4 ; if ax = 4
je ax_is_four ; haz algo si el valor coincide con la condición(salta a dicha etiqueta)
mov bx, 45 ; sería un else
jmp the_end ; indicamos el final del código haciendo que vaya a una etiqueta que no hace nada

ax_is_four:
    mov bx, 23
the_end:
```

En lenguajes como C o Java, se vería tal que así:

```java
if (ax == 4) {
    bx = 23;
} else {
    bx = 45;
}
```

Por google podemos encontrar las [condiciones](https://www.tutorialspoint.com/assembly_programming/assembly_conditions.htm) que existen.

## Funciones

Como hemos dicho antes, una función es sólo un salto a una etiqueta que hayamos definido. Un ejemplo sería:

```asm
mov al, 'X'
jmp print_fuction
endprint:

print_fuction:
    mov ah, 0x0e ; modo tty
    int 0x10 ; Mostrar caracter en al
    jmp endprint
```

El problema con este ejemplo es que la función `print_fuction` sólo volverá a `endprint`, haciendo que no sea posible la reutilización de código.

Para mejorar el código vamos a hacer lo siguiente:

- Guardar la dirección de retorno que puede variar.
- Guardar los registros actuales para permitir que subfunciones puedan modificarlo sin problemas.

Para esto, la CPU lo gestionará. En lugar de usar `jmp`, utilizaremos `call`y `ret` (que vendría a ser como un `return`).

```asm
mov al, 'X'
call print_fuction

print_fuction:
    mov ah, 0x0e
    int 0x10
    ret
```

También tenemos que tener en cuenta si querremos trabajar con el stack y guardar los datos, para lo que usaremos las instrucciones `pusha` y `popa`, que meten y sacan todos los registros hacia y desde el stack, respectivamente.

```asm
some_function:
    pusha ; Mete todos los valores registrados en el stack
    mov bx, 10
    add bx, 20
    mov ah, 0x0e
    int 0x10
    popa ; Restaura los valores originales
    ret
```

## Utilizar ficheros externos

Para simplificar el código y no tener todo en un sólo fichero.

Tenemos que añadir lo siguiente al código:

```asm
%include "file.asm"
```

## Mostrar valores hexadecimales

En el fichero [boot_sect_print_hex.asm](boot_sect_print_hex.asm) extiende el fichero [boot_sect_print.asm](boot_sect_print.asm) para imprimir también hexadecimal, no sólo ASCII.

## Ejemplo

El archivo [boot_sect_main.asm](boot_sect_main.asm) llamará a los dos ficheros mencionados anteriormente para imprimir bytes por pantalla, además de un salto de línea. El salto de línea, `'\n'`, son 2 bytes, el caracter de nueva línea `0x0A` y un retorno de carro `0x0D`.

El fichero principal carga un par de strings y bytes, y las funciones definidas en los ficheros externos.
