# Objetivo: Hacer que el sector de arranque cargue datos desde el disco duro para arrancar el kernel

Para poder trabajar con el disco, tenemos algunas rutinas de la BIOS como utilizamos para imprimir caracteres por pantalla. Para eso configuramos `al` en `0x02`, más otros registros con el cilindro, cabeza y sector (cylinder, head y sector en inglés), y la interrupción `int 0x13`.

Más información sobre 13h [aquí](https://es.wikipedia.org/wiki/Int_13h)

También usaremos el _carry bit_ o _bit de acarreo_, que es un bit extra que hay en cada registro que se almacena cuando una operación ha desbordado su capacidad actual (el me llevo una cuando sumas).

```asm
mov ax, 0xFFFF
add ax, 1 ; ax = 0x0000 y carry = 1
```

No se puede acceder directamente a dicho bit, pero otros operadores lo utilizan como una estructura de control, como `jc` (salta a la condición si se cumple).

La BIOS establece `al` para el número de sectores leídos, así hay que compararlo con el número esperado.

## Código

En el fichero [boot_sect_disk.asm](./boot_sect_disk.asm) está la rutina completa que lee desde el disco.

El fichero [boot_sect_main.asm](./boot_sect_main.asm) prepara los parametros para la lectura de disco y llama a `disk_load`. Vamos a escribir algunos datos que no pertenecen al sector de arranque.

El sector de arranque se encuentra en el sector 1 del cilindro 0 de la cabeza 0 del disco 0, como hemos comentado en el código del disco, por lo que cualquier byte que escribamos después del byte 512, corresponde al sector 2 del cilindro 0 de la cabeza 0 del disco 0.
