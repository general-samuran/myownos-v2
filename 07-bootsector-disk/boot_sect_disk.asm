; Cargamos los sectores 'dh' desde el drive 'dl' en ES:BX

disk_load:
    pusha
    ; leer desde el disco requiere establecer valores en todos los registros
    ; por lo que vamos a sobreescribir 'dx', 
    ; así que vamos a guardarlo para un uso posterior
    push dx

    mov ah, 0x02 ; Función para leer sectores
    mov al, dh   ; Leemos los sectores 'dh'
    mov cl, 0x02 ; Empezamos a leer desde el segundo sector,
                 ; el primero es nuestro sector de arranque
    
    mov ch, 0x00 ; Seleccionamos el cilindro 0
    mov dh, 0x00 ; Elegimos la cabeza 0

    int 0x13 ; Interrupción de la BIOS
    jc disk_error ; Si es error (por el carry bit)

    pop dx
    cmp al, dh ; Comparamos los sectores leídos con los esperados
    jne sectors_error
    popa
    ret

disk_error:
    mov bx, DISK_ERROR
    call print
    call print_nl
    mov dh, ah ; ah = codigo error, dl = disco que ha soltado el error
    call print_hex
    jmp $

sectors_error:
    mov bx, SECTORS_ERROR
    call print

DISK_ERROR: db "Error en el disco", 0
SECTORS_ERROR: db "Numero incorrecto de sectores leídos", 0