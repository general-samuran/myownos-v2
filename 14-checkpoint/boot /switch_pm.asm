[bits 16]
switch_to_pm:
    cli ; 1. Deshabilitar interrupciones
    lgdt [gdt_descriptor] ; 2. Cargar el GDT descriptor
    mov eax, cr0
    or eax, 0x1 ; 3. Configurar el modo 32-bit en cr0
    mov cr0, eax
    jmp CODE_SEG:init_pm ; 4. Saltamos a un nuevo segmento, lejos de nuestro código 32-bit.
                         ; También forzamos a que la CPU limpie la caché

[bits 32]
init_pm:
    mov ax, DATA_SEG ; 5. Actualizamos los registradores de segmento
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov ebp, 0x90000 ; 6. Actualizamos el stack
    mov esp, ebp

    call BEGIN_PM ; 7. Llamamos al código de 32 bits