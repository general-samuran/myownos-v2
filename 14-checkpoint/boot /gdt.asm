; En este código vamos a plasmar la estructura del descriptor de segmento

gdt_start:
    ; GDT comienza con 8 bytes nulos
    dd 0x0  ; 4 bytes
    dd 0x0  ; 4 bytes

; Segmento de código del GDT, lo que comentamos de dividirlo en 2 partes
; base = 0x00000000, limite = 0xfffff
; Primera flag: 1(present) 00(privilige) 1(tipo de descriptor) -> 1001b
; Type flags: 1(code) 0(conforming) 1(que se puede leer/readable) 0(accessed) -> 1010b
; Segunda flag: 1(granularity) 1(32-bit default) 0(64-bit seg) 0(AVL) -> 1100b
gdt_code:
    dw 0xffff ; limite (0-15 bits)
    dw 0x0    ; Base (0-15 bits)
    db 0x0    ; Base (16-32 bits)
    db 10011010b ; Primera flag, flag de tipos
    db 11001111b ; Segunda flag + limite (16-19 bits)
    db 0x0    ; Base (24-31 bits)

; Es el mismo código cambiando los type flags
; Type flags: 0(code) 0(expand down) 1(writable) 0(accessed) -> 0010b
gdt_data:
    dw 0xffff ; limite (0-15 bits)
    dw 0x0    ; Base (0-15 bits)
    db 0x0    ; Base (16-32 bits)
    db 10010010b ; Primera flag, flag de tipos
    db 11001111b ; Segunda flag + limite (16-19 bits)
    db 0x0    ; Base (24-31 bits)

gdt_end:

; GDT Descriptor
gdt_descriptor:
    dw gdt_end - gdt_start - 1 ; El tamaño de nuestra GDT menos 1 es el tamaño real
    dd gdt_start ; Comienzo de la dirección (32 bits) 

; Definimos algunas constantes
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start
