# Objetivo: Organizar un poco mejor el código

Con lo que hemos hecho en el capitulo anterior ya podemos decir que tenemos nuestro propio kernel funcionando, oleee.

En este apartado vamos a organizar el código creando nuevas carpetas para ello, crear un Makefile y pensar en cómo vamos a abordar los siguentes pasos.

Con este nuevo orden de carpetas, también hay que cambiar el código para que apunte a los nuevos ficheros.

En cuanto a cómo será nuestro kernel, haremos un monolito, ya que son más fáciles de diseñar e implementar pero hay muchos más como se muestra en esta [web](https://wiki.osdev.org/Kernels)
