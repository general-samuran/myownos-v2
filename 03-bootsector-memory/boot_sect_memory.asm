mov ah, 0x0e

; 1º intento
; fallará porque está intentando imprimir la dirección de memoria (el puntero),
; no el contenido
mov al, "1"
int 0x10
mov al, the_secret
int 0x10

; 2º intento
; intentará imprimir el contenido que se encuentra en el puntero, que es como tenemos que hacerlo.
; Pero, la BIOS lo va a colocar en el sector de arranque (0x7c00) por lo que tenemos que añadirlo de antes
; tal y como lo haremos en el paso 3
mov al, "2"
int 0x10
mov al, [the_secret]
int 0x10

; 3º intento
; Vamos a agregar la dirección de memoria de la X al inicio del sector de arranque 0x7c00 
; y luego hacemos referencia a ese puntero
; Necesitamos utilizar el registro 'bx' porque utilizar 'mov al, [ax]' es ilegal.
; Un registro no se puede utilizar origen y destino para un mismo comando.
mov al, "3"
int 0x10
mov bx, the_secret
add bx, 0x7c00
mov al, [bx]
int 0x10

; 4º intento
; Utilizamos un atajo ya que sabemos que la X se encuentra en el byte 0x2d de nuestro binario.
; Esto no es eficaz porque tendríamos que contar dónde van etiquetas cada vez que el código cambie.
mov al, "4"
int 0x10
mov al, [0x7c2d]
int 0x10


jmp $ ; Bucle infinito

the_secret:
    ; El código ASCII 0x58 ('X') se guarda antes del relleno con ceros.
    ; Con 'xxd fichero.bin' podremos comprobar que está en el byte 0x2d
    db "X"

; Relleno de ceros y número mágico
times 510-($-$$) db 0
dw 0xaa55
