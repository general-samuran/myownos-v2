# Objetivo: Aprender cómo funciona la memoria de un ordenador

En la página 14 del [siguiente documento](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf#subsection.3.4.2) se nos muestra un diseño de la memoria y dónde se almacena el sector de arranque, en `0x7c00`.

A continuación se van a mostrar 4 ejemplos para intentar entender mejor cómo funciona la memoria, cuáles funcionan y por qué.

Los ejemplos se encuentran en el fichero [boot_sect_memory.asm](./boot_sect_memory.asm).

Primero vamos a definir una 'X' como datos y le asignamos una etiqueta:

```asm
the_secret:
    db "X"
```

Vamos a intentar acceder a `the_secret` de las siguientes formas:

- `mov al, the_secret`
- `mov al, [the_secret]`
- `mov al, the_secret + 0x7c00`
- `mov al, 2d + 0x7c00`, siendo `2d` la posición real del byte 'X' en el binario.

En los comentarios del código se explica más en detalle por qué algunos van a funcionar o no.

En el ejemplo 4 sabemos que X se encuentra en el byte 0x2d, porque está en la fila `00000020` y la columna `d` como se muestra a continuación, siendo un fragmento del binario generado con el código. También sabemos esto porque el código ASCII de X es `58`.

```text
00000000: b40e b031 cd10 b02d cd10 b032 cd10 a02d  ...1...-...2...-
00000010: 00cd 10b0 33cd 10bb 2d00 81c3 007c 8a07  ....3...-....|..
00000020: cd10 b034 cd10 a02d 7ccd 10eb fe58 0000  ...4...-|....X..   <-- Se encuentra en esta fila
                                          ^^ Se encuentra en esta columna
```

## Configuración global

Como tener que declarar `0x7c00` en todas partes es muy engorroso, podemos declararlo globalmente para cada ubicación de memoria con comando `org`.

```asm
[org 0x7c00]
```

En el fichero [boot_sect_memory_org.asm](./boot_sect_memory_org.asm) se puede ver la forma de mostrar datos en el sector de arranque, que ahora es el ejemplo 2.

En los comentarios viene con más detalle de los cambios con y sin `org`
