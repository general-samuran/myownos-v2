[org 0x7c00]
mov ah, 0x0e

; 1º intento
; fallará de nuevo porque sigue intentando imprimir la dirección de memoria (el puntero),
; no el contenido
mov al, "1"
int 0x10
mov al, the_secret
int 0x10

; 2º intento
; Al indicar dónde comienza el sector de arranque, ahora funcionará
mov al, "2"
int 0x10
mov al, [the_secret]
int 0x10

; 3º intento
; Fallará porque estamos añadiendo dos veces 0x7c00
mov al, "3"
int 0x10
mov bx, the_secret
add bx, 0x7c00
mov al, [bx]
int 0x10

; 4º intento
; En este caso nunca se aplica el comando 'org' porque no se hacen referencias de memoria a los punteros.
; Asignar la dirección de memoria contando bytes siempre va a funcionar, aunque no sea conveniente.
mov al, "4"
int 0x10
mov al, [0x7c2d]
int 0x10


jmp $ ; Bucle infinito

the_secret:
    ; El código ASCII 0x58 ('X') se guarda antes del relleno con ceros.
    ; Con 'xxd fichero.bin' podremos comprobar que está en el byte 0x2d
    db "X"

; Relleno de ceros y número mágico
times 510-($-$$) db 0
dw 0xaa55
