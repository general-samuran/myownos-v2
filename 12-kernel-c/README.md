# Objetivo: Aprender cómo escribir código de bajo nivel como hemos hecho con ensamblador pero usando C

## Compilar

Vamos a ver cómo compilar nuestro código en C y compararlo con el código en ensamblador.

En el fichero [function.c](./function.c) hay un sencillo programa con una función.

Para compilar sin que tenga en cuenta el sistema, tenemos que poner la flag `-ffreestanding`, así que vamos a compilar nuestro código así:

```bash
i386-elf-gcc -ffreestanding -c function.c -o function.o
```

Vamos a examinar el objecto generado:

```bash
i386-elf-objdump -d function.o
```

El resultado nos tiene que resultar familiar:

```text

function.o:     file format elf32-i386


Disassembly of section .text:

00000000 <my_function>:
   0:   55                      push   %ebp
   1:   89 e5                   mov    %esp,%ebp
   3:   b8 ba ba 00 00          mov    $0xbaba,%eax
   8:   5d                      pop    %ebp
   9:   c3                      ret                                                               
```

## Enlazar

Para crear un fichero binario, usaremos el linker. Para este ejemplo, colocaremos el desplazamiento en `0x0`, ya que no sabemos dónde se colocará la función en la memoria, y usaremos el formato `binary` para que genere el código máquina sin etiquetas ni metadatos.

```bash
i386-elf-ld -o function.bin -Ttext 0x0 --oformat binary function.o
```

Si examinamos el fichero `function.bin` con el comando `xxd`, veremos el código máquina del mismo, pero sin todas las etiquetas e información de depuración que contiene el fichero `function.o`

```text
00000000: 5589 e5b8 baba 0000 5dc3                 U.......].
```

## Decompilar

Como curiosidad, podemos examinar el código máquina del fichero `function.bin` de la siguente manera.

```bash
ndisasm -b 32 function.bin 
```

Con el siguiente resultado:

```text
00000000  55                push ebp
00000001  89E5              mov ebp,esp
00000003  B8BABA0000        mov eax,0xbaba
00000008  5D                pop ebp
00000009  C3                ret
```

## Ejemplos varios

En los siguientes ficheros veremos más casos de programación en C:

- Variables locales [localvars.c](./localvars.c)
- Llamadas a funciones [functioncalls.c](./functioncalls.c)
- Punteros [pointers.c](./pointers.c)
