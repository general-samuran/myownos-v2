/**
 * Lee un byte del puerto especificado
*/

unsigned char port_byte_in(unsigned short port){
    unsigned char result;
    /** Sintaxis de ensamblador
     * Los registros de origen y destino se cambian a NASM
     * '"=a" (result)' el '=' configura la variable '(result)' al valor del registro eax
     * '"d" (port)' mapea la variable '(port)' al registro edx
     * 
     * Las entradas y salidas están separadas por dos puntos
    */
   __asm__("in %%dx, %%al" : "=a" (result) : "d" (port));
   return result;
}

void port_byte_out(unsigned short port, unsigned char data){
    /** Aquí los registros se asignan a variables en C y no devuelven nada,
     * no es igual al '=' que hemos utilizado en la sintaxis de asm.
     * Utilizamos una coma ya que hay dos variables de entrada pero ninguna de salida,
     * o que devolver con el 'return' 
    */
    __asm__("out %%al, %%dx" : : "a" (data), "d" (port));
}

unsigned short port_word_in(unsigned short port){
    unsigned short result;
    __asm__("in %%dx, %%ax" : "=a" (result) : "d" (port));
    return result;
}

void port_word_out(unsigned short port, unsigned char data){
    __asm__("out %%ax, %%dx" : : "a" (data), "d" (port));
}