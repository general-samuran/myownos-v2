; Vamos a recibir los datos en 'dx'
; En este ejemplo usaremos el valor '0x1234'

print_hex:
    pusha
    mov cx, 0 ; nuestro indice

; Vamos a obtener el último valor de 'dx' y convertirlo a ASCII
; Los valores númerios ASCII son: '0' (0x30) a '9' (0x39), por lo que tendremos que agregar 0x30 a byte N.
; Para los valores alfabeticos A-F: 'A' (0x41) a 'F' (0x46), por lo que agregaremos 0x40
; Luego hay que mover el byte ASCII a la posición correcta en el string resultante

hex_loop:
    cmp cx, 4 ; hacemos el loop 4 veces
    je end

    ; Convertimos el último caracter de 'dx' a ascii
    mov ax, dx ; vamos a usar 'ax' como nuestro registro
    and ax, 0x000f ; 0x1234 -> 0x0004, hacemos que los 3 primeros digitos se conviertan a cero
    add al, 0x30 ; añadimos 0x30 a N para convertirlo en ASCII
    cmp al, 0x39 ; compara el caracter ASCII con '9'
    jle step2
    add al, 7     ; Si el carácter es mayor a '9', se le agrega 7 para convertirlo en una letra hexadecimal

step2:
    ; Vamos a obtener la posición correcta del string para colocar nuestro caracter ASCII
    ; bx <- posición base + longitud string - indice de char
    mov bx, HEX_OUT + 5 ; base + longitud
    sub bx, cx ; nuestro indice
    mov [bx], al ; copiamos el caracter ASCII en 'al' a la posición señalada por 'bx'
    ror dx, 4 ; Rota 4 veces, 0x1234 -> 0x4123 -> 0x3412 -> 0x2341 -> 0x1234

    ; incrementamos el indice y otra vez al loop
    add cx, 1
    jmp hex_loop

end:
    ; preparamos el parametro y llamamos a la función
    ; recordemos que print recibe los parametros en 'bx'
    mov bx, HEX_OUT
    call print

    popa
    ret

HEX_OUT:
    db '0x0000', 0 ; Reservamos memoria para nuestra string