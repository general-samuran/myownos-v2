[bits 32]
[extern main] ; Definimos el punto de llamada. Tiene que tener el 
              ; mismo nombre que la función 'main' del código kernel.c
call main     ; Llamamos a la función de C. El linker sabrá dónde está en la memoria
jmp $