#include "../drivers/ports.h"

void main(){
    /* Posicion del cursor de la pantalla: Pregunta al registro de control del VGA (0x3d4) por los
    * bytes 14 = el byte superior del cursor y 15 = el byte inferior del cursor. */ 
   port_byte_out(0x3d4, 14); /* Solicitamos el byte 14: el byte superior del cursor*/
   int position = port_byte_in(0x3d5);
   position = position << 8; /* byte superior*/

   port_byte_out(0x3d4, 15); /* Pidiendo el byte inferior*/
   position += port_byte_in(0x3d5);

   /* Las 'celdas' VGA consisten en el carácter y sus datos, por ejemplo, 'blanco sobre fondo negro', 'texto rojo sobre fondo blanco', etc */
   int offset_from_vga = position * 2;

   /* Vamos a escribir en la posición actual del cursor, como ya hicimos antes */
   char *vga = 0xb8000;
   vga[offset_from_vga] = 'X';
   vga[offset_from_vga+1] = 0x0f; /* Texto blanco sobre fondo negro*/
}