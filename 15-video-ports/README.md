# Objetivo: Aprender a usar los puertos de datos de VGA

Usaremos C para comunicarnos con los dispositivos a través de registros y puertos de I/O.

En fichero [drivers/ports.c](./drivers/ports.c) estará el código necesario para empezar.

Una vez terminado de revisar ese código, vamos a ver el fichero [kernel/kernel.c](./kernel/kernel.c), que hemos cambiado para integrarlo con el anterior código.

En este ejemplo usaremos el puerto `0x3d4` con el valor `14` para solicitar el byte superior de la posición del cursor, y en el mismo puerto con `15` para el byte inferior.

Cuando consulta este puerto, guarda el resultado en el puerto 0x3d5.

Finalmente usaremos la posición del cursor para escribir un carácter en esa posición.
