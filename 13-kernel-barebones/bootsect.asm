[org 0x7c00]
KERNEL_OFFSET equ 0x1000 ; El valor tiene que ser el mismo que el usado para linkear el kernel

    mov [BOOT_DRIVE], dl ; La BIOS establece la unidad de arranque en 'dl'
    mov bp, 0x9000
    mov sp, bp

    mov bx, MSG_REAL_MODE
    call print
    call print_nl

    call load_kernel ; leemos el kernel del disco
    call switch_to_pm ; deshabilitamos interrupciones, cargamos GDT, etc. Finalmente saltamos a 'BEGIN_PM'
    jmp $ ; Realmente nunca se ejecuta

%include "../05-bootsector-functions-strings/boot_sect_print.asm"
%include "../05-bootsector-functions-strings/boot_sect_print_hex.asm"
%include "../07-bootsector-disk/boot_sect_disk.asm"
%include "../09-32bit-gdt/32bit-gdt.asm"
%include "../08-32bit-print/32bit-print.asm"
%include "../10-32bit-enter/32bit-switch.asm"

[bits 16]
load_kernel:
    mov bx, MSG_LOAD_KERNEL
    call print
    call print_nl

    mov bx, KERNEL_OFFSET ; Lee del disco y almacena en 0x1000
    mov dh, 2
    mov dl, [BOOT_DRIVE]
    call disk_load
    ret

[bits 32]
BEGIN_PM:
    mov ebx, MSG_PROT_MODE
    call print_string_pm
    call KERNEL_OFFSET ; Damos el control al kernel
    jmp $ ; Que se mantenga aquí cuando el kernel nos devuelva el control (si lo hace)

BOOT_DRIVE db 0 ; Lo almacenamos en memoria porque lo más probable es que 'dl' sea sobreescrito
MSG_REAL_MODE db "Iniciado modo real 16-bit", 0
MSG_PROT_MODE db "Cargado modo protegido 32-bit", 0
MSG_LOAD_KERNEL db "Kernel cargado en memoria", 0

times 510-($-$$) db 0
dw 0xaa55