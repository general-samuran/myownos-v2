# Objetivo: Crear un kernel simple y un bootloader capaz de arrancarlo

## Kernel

Nuestro kernel en C va a imprimir una X en la esquina superior izquierda de la pantalla, el fichero es [kernel.c](./kernel.c)

La función del principio nos obligará a crear una rutina de entrada al kernel que no apunte al byte 0x0, sino a una etiqueta real que sabemos que lo inicia, en este caso la función `main()`.

Compilamos el código C:

```bash
i386-elf-gcc -ffreestanding -c kernel.c -o kernel.o
```

La rutina mencionada está en [kernel_entry.asm](./kernel_entry.asm). En el código usaremos `[extern]`. Para compilar este código, en vez de generar un binario, vamos a generar un archivo `elf` que se vinculará con `kernel.o`.

```bash
nasm kernel_entry.asm -f elf -o kernel_entry.o
```

`ELF` son las siglas de Executable and Linking Format.

## Linker

Tenemos que vincular ambos archivos en un sólo fichero binario y resolver las referencias de etiquetas que hay, para ello ejecutamos:

```bash
i386-elf-ld -o kernel.bin -Ttext 0x1000 kernel_entry.o kernel.o --oformat binary
```

Nuestro kernel no se colocará en `0x0` en la memoria, sino en `0x1000`, el sector de arranque también necesitará conocer esa dirección. También es importante el orden de los ficheros según los ponemos en el comando, `kernel_entry.o` tiene que ir antes .que `kernel.o`

## Sector de arranque

Es muy similar al de la lección 10. El fichero es [bootsect.asm](./bootsect.asm), aunque se puede eliminar las líneas usadas para imprimir mensajes por pantalla, quedando en un código de un par de docenas de líneas.

Lo compilamos con:

```bash
nasm bootsect.asm -f bin -o bootsect.bin
```

## Juntar todo

Ahora que tenemos el kernel y el bootsector, hay que juntarlo simplemente concatenandolos:

```bash
cat bootsect.bin kernel.bin > os-image.bin
```

## Ejecutarlo

Ahora podemos ejecutar la imagen `os-image.bin` con qemu.

Para que no dé problemas a la hora de cargarlo por el disco, tenemos que usar el comando `qemu -fda os-image.bin`, sino nos dará un error.

Si funciona, veremos 4 mensajes:

- "Iniciado modo real 16bit"
- "Kernel cargado en memoria"
- "Cargado modo protegido 32-bit"
- Una "X" sobreescribiendo el mensaje anterior.

## Makefile

Hacer todo el proceso hasta obtener el componente final puede llegar a ser engorroso, por lo que vamos a utilizar un fichero [`Makefile`](./Makefile), como el que hay en la raíz del repositorio, que lo utilizo para hacer una limpieza general de los ficheros generado
