# Objetivo: escribir texto por pantalla

Vamos a mostrar algo de texto por la pantalla. En esta lección va a haber un poco más de código que lo normal.

En el fichero [drivers/screen.h](./drivers/screen.h) hay definidas varias constantes y 3 funciones publicas, una para limpiar la pantalla y otras dos para poder escribir texto, `kprint` de "kernel print".

En el fichero [drivers/screen.c](./drivers/screen.c) empezamos declarando funciones privadas que nos ayudarán a nuestra función `kprint`.

Hay dos rutinas de acceso al puerto de E/S que utilizamos la lección anterior, `get` y `set_cursor_offset()`.

Luego está la rutina que manipula directamente la memoria de video, `print_char()`.

Finalmente hay 3 funciones que transforman filas y columnas en desplazamientos y viceversa.

## kprint_at

`kprint_at` se puede llamar con un valor `-1` para `col` y `row`, lo que indica que imprimiremos el texto en la posición actual del cursor.

Primero establecemos 3 variables para col/row y el desplazamiento. Luego itera a través de `*char` y llama a `print_char()` con las coordenadas actuales.

Hay que tener en cuenta que `print_char` por si mismo devuelve el desplazamiento de la siguiente posición del cursor y lo reutilizamos para el siguiente loop.

`kprint` es un envoltorio para `kprint_at`.

## print_char

Al igual que `kprint_at`, esta función permite que las columnas/filas sean `-1`. En este caso recupera la posición del cursor del hardware, usando las rutinas de `ports.c`.

`print_char` también maneja saltos de línea. En este caso, colocamos el desplazamiento del cursor en la columna 0 de la siguiente fila.

Las celdas VGA toman 2 bytes, uno para el carácter en sí y otro para el atributo.

## kernel.c

Nuestro nuevo kernel finalmente puede imprimir texto.

Se comprueba que coloque bien los carácteres, los saltos de línea, las líneas multiples y si se escribe fuera de la pantalla.
