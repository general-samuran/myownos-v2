#include "screen.h"
#include "ports.h"

/* Declaramos la funciones privadas */
int get_cursor_offset();
void set_cursor_offset(int offset);
int print_char(char c, int col, int row, char attr);
int get_offset(int col, int row);
int get_offset_row(int offset);
int get_offset_col(int offset);

/* Funciones publicas */

/**
 * Escribe el mensaje en la localización indicada,
 * si col o row son negativos, usará el desplazamiento actual
*/

void kprint_at(char *message, int col, int row){
    /* Configura el cursor si col/row son negativos*/
    int offset;
    if (col >=0 && row >=0)
        offset = get_offset(col, row);
    else {
        offset = get_cursor_offset();
        row = get_offset_row(offset);
        col = get_offset_col(offset);
    }

    /* Recorremos el mensaje y lo pintamos */
    int i = 0;
    while (message[i] != 0) {
        offset = print_char(message[i++], col, row, WHITE_ON_BLACK);
        /* Calcula row/col para la siguente interacción */
        row = get_offset_row(offset);
        col = get_offset_col(offset);
    }
}

void kprint(char *message){
    kprint_at(message, -1, -1);
}

/* Funciones privadas */

/**
 * Función interna para nuestro kernel, accede directamente a la memoria de video
 * Si 'col' o 'row' son negativos, imprimiremos en la ubicación actual del cursor
 * Si 'attr' es cero, usará 'WHITE_ON_BLACK' por defecto
 * Devuelve el desplazamiento del siguiente carácter
 * Configura el cursor del vídeo en el desplazamiento devuelto
*/

int print_char(char c, int col, int row, char attr){
    unsigned char *vidmem = (unsigned char*) VIDEO_ADDRESS;
    if (!attr) attr = WHITE_ON_BLACK;

    /* Control de errores: pinta una 'E' roja cuando las coordenadas no son correctas*/
    if (col >= MAX_COLS || row >= MAX_ROWS){
        vidmem[2*(MAX_COLS)*(MAX_ROWS)-2] = 'E';
        vidmem[2*(MAX_COLS)*(MAX_ROWS)-1] = RED_ON_WHITE;
        return get_offset(col, row);
    }

    int offset;
    if (col >=0 && row >=0) offset = get_offset(col, row);
    else offset = get_cursor_offset();

    if (c == '\n'){
        row = get_offset_row(offset);
        offset = get_offset(0, row+1);
    } else {
        vidmem[offset] = c;
        vidmem[offset+1] = attr;
        offset += 2;
    }
    set_cursor_offset(offset);
    return offset;
}

int get_cursor_offset(){
    /**
     * Usa los puertos VGA para obtener la posición actual del cursor
     * 1. Pregunta por el byte superior del cursor (data 14)
     * 2. Pregunta por el byte inferior (data 15)
    */
   port_byte_out(REG_SCREEN_CTRL, 14);
   int offset = port_byte_in(REG_SCREEN_DATA) << 8;
   port_byte_out(REG_SCREEN_CTRL, 15);
   offset += port_byte_in(REG_SCREEN_DATA);
   return offset * 2; /* Posición * el tamaño de la celda del caracter */
}

void set_cursor_offset(int offset){
    /* Similar a get_cursor_offset, pero en vez de leer, escribimos datos */
    offset /= 2;
    port_byte_out(REG_SCREEN_CTRL, 14);
    port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset >> 8));
    port_byte_out(REG_SCREEN_CTRL, 15);
    port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset & 0xff));
}

void clear_screen(){
    int screen_size = MAX_COLS * MAX_ROWS;
    int i;
    char *screen = VIDEO_ADDRESS;

    for(i=0; i<screen_size; i++){
        screen[i*2] = ' ';
        screen[i*2+1] = WHITE_ON_BLACK;
    }
    set_cursor_offset(get_offset(0,0));
}

int get_offset(int col, int row){ return 2 * (row * MAX_COLS + col);}
int get_offset_row(int offset){ return offset / (2 * MAX_COLS);}
int get_offset_col(int offset){ return (offset - (get_offset_row(offset)*2*MAX_COLS))/2;}
